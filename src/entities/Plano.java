/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author cesar.vega-f
 */
public class Plano {

    int[][] xy;

    public int getValue(int x, int y) {
        return xy[x - 1][y - 1];
    }

    public void setValue(int x, int y, int w) {
        this.xy[x - 1][y - 1] = w;
    }

    public int getlength() {
        return xy.length;
    }

    public Plano(int N) {
        xy = new int[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                xy[i][j] = 0;
            }
        }
    }

}
