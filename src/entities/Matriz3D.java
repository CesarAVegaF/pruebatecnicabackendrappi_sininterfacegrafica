/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.ArrayList;

/**
 *
 * @author cesar.vega-f
 */
public class Matriz3D {

    ArrayList<Plano> matriz; // Objeto de representación de la matriz se toma como referencia el z para la indexacion de la 3D

    public ArrayList getMatriz() {
        return matriz;
    }

    public void setMatriz(ArrayList matriz) {
        this.matriz = matriz;
    }

    /**
     * Crear la matriz de N*N*N usando objetos tipo plano XY
     *
     * @param N número de lados de la matriz
     */
    public void create(int N) {
        matriz = new ArrayList();
        for (int i = 0; i < N; i++) {
            matriz.add(new Plano(N));
        }
    }

    public void setValue(int x, int y, int z, int W) {
        matriz.get(z - 1).setValue(x, y, W);
    }

    public int getValue(int x, int y, int z) {
        return matriz.get(z - 1).getValue(x, y);
    }

    /**
     * Obtiene la suma de los valores acumulados en la matriz
     * @param x1 coordenada
     * @param y1 coordenada
     * @param z1 coordenada
     * @param x2 coordenada
     * @param y2 coordenada
     * @param z2 coordenada
     * @return valor de la sumatoria entre las coordenadas establecidas
     */
    public int getQuery(int x1, int y1, int z1, int x2, int y2, int z2) {
        int sum = 0;
        Plano planoTemp;
        for (int z = z1; z <= z2; z++) {
            planoTemp = matriz.get(z - 1);
            for (int x = x1; x <= x2; x++) {
                for (int y = y1; y <= y2; y++) {
                    sum += planoTemp.getValue(x, y);
                }
            }
        }
        return sum;
    }

}
