/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebatecnicabackendrappi;

import entities.Matriz3D;
import entities.Plano;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import pruebaTecnicaBackendRappiExcepcion.PruebaTecnicaBackendRappiExcepcion;
import java.util.ArrayList;

/**
 * Clase principal de aplicación lógica
 *
 * @author cesar.vega-f
 */
public class PruebaTecnicaBackendRappi {

    private static int pint(String string) {
        return Integer.parseInt(string);
    }

    private Matriz3D mat3D;
    private static int WMAX = (int) Math.pow(10.0, 9.0);

    /**
     * @param args datos de entrada por consola
     */
    public static void main(String[] args) throws IOException, PruebaTecnicaBackendRappiExcepcion {
        
        // Lectura e impresion de información
        BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter out = new PrintWriter(new OutputStreamWriter(System.out));
        
        // Número de casos
        int T = Integer.parseInt(bf.readLine());
        Matriz3D mat3D = new Matriz3D();
        while (T-- > 0){
            String[] nm = bf.readLine().split("\\s+");
            int N = pint(nm[0]); // Tamaño de la matriz
            int M = pint(nm[1]); // Número de consultas
            mat3D.create(N); // se crea la matriz
            String[] temp; // Arreglo de lectura
            for (int i = 0; i < M; i++){
                temp = bf.readLine().split("\\s+");
                if(temp[0].equals("UPDATE")){
                    mat3D.setValue(pint(temp[1]), pint(temp[2]), pint(temp[3]), pint(temp[4]));
                } else{
                    // Se agrega al Buffer de salida la respuesta de la consulta
                    out.append(mat3D.getQuery(pint(temp[1]), pint(temp[2]), pint(temp[3]), pint(temp[4]), pint(temp[5]), pint(temp[6]))+"\n");
                }
            }
        }
        
        //Cierre
        bf.close();
        out.close();
        
    }

    /**
     * Función para la creación de la matriz según el tamaño N
     *
     * @param N valor de longitud para la creación de la matriz
     * @throws
     * pruebaTecnicaBackendRappiExcepcion.PruebaTecnicaBackendRappiExcepcion
     */
    public void crearMatriz3D(int N) throws PruebaTecnicaBackendRappiExcepcion {
        if (0 < N && N <= 100) {
            mat3D = new Matriz3D();
            mat3D.create(N);
        } else {
            throw new PruebaTecnicaBackendRappiExcepcion("Valor de los rangos incorrectos: N");
        }
    }

    /**
     * Obtiene el objeto matriz 3D
     * @return ArrayList<Plano> equivalente a una matriz 3D
     */
    public ArrayList<Plano> getMatriz3D() {
        return mat3D.getMatriz();
    }

    /**
     * Actualizar el valor de una casilla determinada en la matriz 3D
     *
     * @param x coordenada
     * @param y coordenada
     * @param z coordenada
     * @param W nuevo valor de la casilla
     * @throws
     * pruebaTecnicaBackendRappiExcepcion.PruebaTecnicaBackendRappiExcepcion
     */
    public void updateMatriz(int x, int y, int z, int W) throws PruebaTecnicaBackendRappiExcepcion {
        if (esRangoCorrecto(x, y, z)
                && (-1 * WMAX) <= W && W <= WMAX) {
            mat3D.setValue(x, y, z, W);
        } else {
            throw new PruebaTecnicaBackendRappiExcepcion("Valor de los rangos incorrectos: Coordenadas");
        }

    }

    /**
     * Obtiene el valor de una casilla determinada en la matriz
     *
     * @param x coordenada
     * @param y coordenada
     * @param z coordenada
     * @return valor de la casilla
     * @throws PruebaTecnicaBackendRappiExcepcion
     */
    public int getValue(int x, int y, int z) throws PruebaTecnicaBackendRappiExcepcion {
        if (esRangoCorrecto(x, y, z)) {
            return mat3D.getValue(x, y, z);
        } else {
            throw new PruebaTecnicaBackendRappiExcepcion("Valor de los rangos incorrectos");
        }

    }

    public int queryMatriz(int x1, int y1, int z1, int x2, int y2, int z2) throws PruebaTecnicaBackendRappiExcepcion {
        if (esRangoCorrecto(x1, y1, z1) && esRangoCorrecto(x2, y2, z2)
                && x1 <= x2 && y1 <= y2 && z1 <= z2) {
            return mat3D.getQuery(x1, y1, z1, x2, y2, z2);
        } else {
            throw new PruebaTecnicaBackendRappiExcepcion("Valor de los rangos incorrectos");
        }
    }

    /**
     * Verifica que los valores estén dentro del rango de condiciones del
     * problema
     *
     * @param x coordenada
     * @param y coordenada
     * @param z coordenada
     * @return
     */
    private boolean esRangoCorrecto(int x, int y, int z) {
        int mat3DSize = mat3D.getMatriz().size();
        return (0 < x && x <= mat3DSize) && (0 < y && y <= mat3DSize) && (0 < z && z <= mat3DSize);
    }

}
