/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import entities.Matriz3D;
import entities.Plano;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import pruebaTecnicaBackendRappiExcepcion.PruebaTecnicaBackendRappiExcepcion;
import pruebatecnicabackendrappi.PruebaTecnicaBackendRappi;

/**
 * Clase de pruebas unitarias por funcionalidad
 *
 * @author cesar.vega-f
 */
public class PruebaTecnicaBackendRappiJUnitTest {

    PruebaTecnicaBackendRappi ptbr;

    public PruebaTecnicaBackendRappiJUnitTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ptbr = new PruebaTecnicaBackendRappi();
    }

    @After
    public void tearDown() {
    }

    /**
     * Creación correcta de la matriz 3D, dentro de los rangos.
     */
    @Test
    public void creacionMatriz3D() {
        try {
            int N = (int) (Math.random() * 100) + 1;
            ptbr.crearMatriz3D(N);
            ArrayList<Plano> matriz = ptbr.getMatriz3D(); // traer la matriz creada
            assertTrue("No se creó correctamente", matriz.size() == N); // verifica que el rango sea el correcto
            for (Plano xy : matriz) {
                assertTrue("No se creó correctamente", xy.getlength() == N);
                for (int i = 1; i <= N; i++) {
                    for (int j = 1; j <= N; j++) {
                        assertTrue("No se creó correctamente", xy.getValue(i, j) == 0); // verifica que la matriz fue creada con valores de 0
                    }
                }
            }
        } catch (PruebaTecnicaBackendRappiExcepcion ex) {
            Logger.getLogger(PruebaTecnicaBackendRappiJUnitTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Actualiza los valores correctamente en la matriz
     */
    @Test
    public void funcionUpdate() {
        try {
            int N = (int) (Math.random() * 100) + 1;
            ptbr.crearMatriz3D(N);
            int x = (int) (Math.random() * N) + 1;
            int y = (int) (Math.random() * N) + 1;
            int z = (int) (Math.random() * N) + 1;
            int W = (int) (Math.random() * Math.pow(10.0, 9.0));
            W = (Math.random() <= 0.50) ? W : -1 * W;
            ptbr.updateMatriz(x, y, z, W);
            assertTrue("Valor incorrecto", ptbr.getValue(x, y, z) == W); // Dentro de la matriz
        } catch (PruebaTecnicaBackendRappiExcepcion ex) {
            Logger.getLogger(PruebaTecnicaBackendRappiJUnitTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * No actualiza el valor en la matriz dado los datos incorrectos, tanto en
     * la actualización como en la consulta
     */
    @Test
    public void noActualizaDatos() {

        int N = (int) (Math.random() * 100) + 1;
        try {
            ptbr.crearMatriz3D(N);
        } catch (PruebaTecnicaBackendRappiExcepcion ex) {
            Logger.getLogger(PruebaTecnicaBackendRappiJUnitTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        int x = 0;
        int y = 0;
        int z = 0;
        int W = (int) (Math.random() * Math.pow(10.0, 9.0));
        W = (Math.random() <= 0.50) ? W : -1 * W;

        try {
            ptbr.updateMatriz(x, y, z, W);
        } catch (PruebaTecnicaBackendRappiExcepcion ex) {
            assertTrue("Actualizó los valores", true);
        }
        try {
            ptbr.getValue(0, 0, 0);
        } catch (PruebaTecnicaBackendRappiExcepcion ex) {
            assertTrue("Actualizó los valores", true);
        }
    }

    /**
     * Probar funcionalidad de consulta, y trayendo la sumatoria en un rango
     * valido
     */
    @Test
    public void funcionQuery() {
        try {
            Matriz3D temp = new Matriz3D();
            int N = (int) (Math.random() * 100) + 1;
            ptbr.crearMatriz3D(N);
            temp.create(N); // copia de comparación 
            int x, y, z, W;
            for (int i = 0; i < 100; i++) {
                x = (int) (Math.random() * N) + 1;
                y = (int) (Math.random() * N) + 1;
                z = (int) (Math.random() * N) + 1;
                W = (int) (Math.random() * Math.pow(10.0, 9.0));
                W = (Math.random() <= 0.50) ? W : -1 * W;

                ptbr.updateMatriz(x, y, z, W);

                temp.setValue(x, y, z, W);

            }
            //Matriz total
            assertTrue("No son iguales las sumas", ptbr.queryMatriz(1, 1, 1, N, N, N) == temp.getQuery(1, 1, 1, N, N, N));

            //Prueba parcial
            x = (int) (Math.random() * N) + 1;
            y = (int) (Math.random() * N) + 1;
            z = (int) (Math.random() * N) + 1;

            assertTrue("No son iguales las sumas", ptbr.queryMatriz(x, y, z, N, N, N) == temp.getQuery(x, y, z, N, N, N));

        } catch (PruebaTecnicaBackendRappiExcepcion ex) {
            Logger.getLogger(PruebaTecnicaBackendRappiJUnitTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Caso de prueba de la pagina
     */
    @Test
    public void pruebaPagina() {
        try {
            int N = 4;
            ptbr.crearMatriz3D(N);
            ptbr.updateMatriz(2, 2, 2, 4);
            assertTrue("Valor no coincide: query 1", ptbr.queryMatriz(1, 1, 1, 3, 3, 3) == 4);
            ptbr.updateMatriz(1, 1, 1, 23);
            assertTrue("Valor no coincide: query 2", ptbr.queryMatriz(2, 2, 2, 4, 4, 4) == 4);
            assertTrue("Valor no coincide: query 3", ptbr.queryMatriz(1, 1, 1, 3, 3, 3) == 27);
            
            N = 2;
            ptbr.crearMatriz3D(N);
            ptbr.updateMatriz(2, 2, 2, 1);
            assertTrue("Valor no coincide: query 4", ptbr.queryMatriz(1, 1, 1, 1, 1, 1) == 0);
            assertTrue("Valor no coincide: query 5", ptbr.queryMatriz(1, 1, 1, 2, 2, 2) == 1);
            assertTrue("Valor no coincide: query 6", ptbr.queryMatriz(2, 2, 2, 2, 2, 2) == 1);
        } catch (PruebaTecnicaBackendRappiExcepcion ex) {
            Logger.getLogger(PruebaTecnicaBackendRappiJUnitTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
